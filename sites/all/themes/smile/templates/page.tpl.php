<div class="blue-header">
<div class="smile-header clearfix">
  <div class="logo-div">
    <?php if ($logo): ?>
      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
        <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
      </a>
    <?php endif; ?>
  </div>
  <div class="smile-main-menu">
    <?php print render($page['main_menu']); ?>
  </div>

<div class="main-hedding">
   <?php print theme('links__system_secondary_menu', array('links' => $secondary_menu, 'attributes' => array('id' => 'secondary-menu', 'class' => array('links', 'inline', 'clearfix')), 'heading' => t('Secondary menu'))); ?>
  <?php print render($page['main-hedding']); ?>
</div>

</div>
</div>

<div class="smile-content-container-div clearfix">
  <?php if ($messages): ?>
    <div id="messages">
      <div class="section clearfix">
        <?php print $messages; ?>
      </div>
    </div>
  <?php endif; ?>
   
  <?php if ($breadcrumb): ?>
    <div id="breadcrumb"><?php print $breadcrumb; ?></div>
  <?php endif; ?>
   
  <h1><?php print $title; ?></h1>
   
  <?php print render($page['content']); ?>
</div>
<div class="smile-footer">
  <hr />
  <div class="smile-footer-text">
    &copy; 2015 smile. All rights reserved.
  </div>
</div>