<div class="main">
 <div class="container">    
  <div class="row"> 
    <div class="header">
    <div class="col-sm-12 col-xs-12 col-md-offset-1 col-md-4 ">
     <h2 ui-sref="front.hp">tapdaq</h2>
    </div>

    <div class="col-sm-12 col-xs-12 col-md-7 ">
     <nav class="navbar navbar-default">
      <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
       <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
       </div>

       <!-- Collect the nav links, forms, and other content for toggling -->
       <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <li><a href="#">Home</a></li>
          <li><a href="#">About</a></li>
          <li><a href="#">Team</a></li>
          <li><a href="#">Blog</a></li>
          <li><a href="#">Support</a></li>
          <li><a href="#">Contact</a></li>
        </ul>
       <form class="navbar-form navbar-left" role="search">
         <button type="submit" class="btn btn-default">Login</button>
       </form>
  
       </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
     </nav>
    </div>
   </div>
  </div>

   <div class="mid-content">
    <div class="row"> 
     <div class="col-sm-12 col-xs-12 col-md-12 ">
      <?php print render($page['hed1']); ?>
    </div>
  </div>
</div>

   <div class="signup"> 
    <div class="row"> 
     <div class="col-sm-12 col-xs-12  col-md-offset-4 col-md-2 "><br>
      <button type="submit" class="btn btn-default">SIGN UP</button>
      <i class="divider-or">&nbsp &nbsp or &nbsp </i></div>
      <div class="col-sm-12 col-xs-12  col-md-6"><br><br>
      <a class="link" href="#">Learn More About Tapdaq</a>
    </div>
    
  </div>
</div>

 </div>
</div>

<div class="how">
  <div class="container">
   <div class="row"> 
     <div class="col-sm-12 col-xs-12 col-md-12 ">
      <span class="label">HOW TAPDAQ WORKS</span>
      <h3 class="mid-title">We are beautifully simple, yet immensely effective</h3>
    </div>
  </div>
  <div class="row"> 
     <div class="col-sm-12 col-xs-12 col-md-4">
      <img src="images/01.png"><br><br><br>
      <h4>Integrate Our SDK</h4>
      <p>Install our SDK, upload your creatives and then submit your application to Google Play or The App Store.</p>
    </div>
    <div class="col-sm-12 col-xs-12 col-md-4">
      <img src="images/02.png"><br><br><br>
      <h4>Generate Installs</h4>
      <p>Through our marketplace, quickly browse thousands of applications, trade installs with other developers and watch your user base grow.</p>
    </div>
    <div class="col-sm-12 col-xs-12 col-md-4">
      <img src="images/03.png"><br><br><br>
      <h4>Improve Your App</h4>
      <p>Use our tools to track your average revenue per user and talk to the community at Tapdaq for feedback on how you can improve your application.</p>
    </div>
</div>
</div>